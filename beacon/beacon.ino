#include "kadyrovlcd.h"

KadyrovLcd lcd(KadyrovLcd::Address::NEW);

#define PACKET_LEN 7
#define NUMS_COUNT 4

#define LIFT_PIN_A 11
#define LIFT_PIN_B 12

#define LIFT_WAIT_TIME 1000
enum class Packet {
    Invalid = 0,
    Number,
    StartLift,
};

static int mults[] = {1000, 100, 10, 1};

Packet endType(byte *term)
{
    if (term[0] == 0xff && term[1] == 0xff && term[2] == 0xff) {
        return Packet::Number;
    } else if (term[0] == 0xfa && term[1] == 0xfa && term[2] == 0xfa) {
        return Packet::StartLift;
    } else {
        return Packet::Invalid;
    }
}

Packet bufType(byte buf[7])
{
    return endType(&buf[4]);
}

void recover()
{
    Serial.println(F("Recover!"));
    auto av = Serial.available();
    byte buf[av];
    Serial.readBytes(buf, av);
}

namespace Lift {
void stop()
{
    digitalWrite(LIFT_PIN_A, HIGH);
    digitalWrite(LIFT_PIN_B, HIGH);
}

void up()
{
    digitalWrite(LIFT_PIN_A, HIGH);
    digitalWrite(LIFT_PIN_B, LOW);
}

void down()
{
    digitalWrite(LIFT_PIN_A, LOW);
    digitalWrite(LIFT_PIN_B, HIGH);
}
}

void setup()
{   
    pinMode(LIFT_PIN_A, OUTPUT);
    pinMode(LIFT_PIN_B, OUTPUT);
    Lift::stop();
    Serial.begin(9600);
    lcd.setup();
    delay(500);
}

void startLift(uint16_t timeUpDown[2])
{
    Lift::up();
    delay(timeUpDown[0]);
    Lift::stop();
    delay(LIFT_WAIT_TIME);
    Lift::down();
    delay(timeUpDown[1]);
    Lift::stop();
}

int last = 0;
void loop()
{
    if (Serial.available() >= PACKET_LEN) {
        Serial.println(F("Read!"));
        byte buf[PACKET_LEN];
        Serial.readBytes(buf, PACKET_LEN);
        auto pacT = bufType(buf);
        if (pacT == Packet::Invalid) {
            Serial.println(F("Invalid buffer!"));
            recover();
            return;
        } else if (pacT == Packet::Number) {
            int finalNum = 0;
            for (int i = 0; i < NUMS_COUNT; ++i) {
                finalNum += mults[i] * buf[i];
            }
            if (finalNum != last) {
                Serial.println(F("Print!"));
                lcd.print(finalNum);
            }
            last = finalNum;
        } else if (pacT == Packet::StartLift) {
            union {
                byte array[4];
                uint16_t timeUpDown[2];
            } converter;
            for (int i = 0; i < 4; ++i)
                converter.array[i] = buf[i];
            Serial.println(F("Lifting for Up/Down: "));
            Serial.println(converter.timeUpDown[0]);
            Serial.println(converter.timeUpDown[1]);
            startLift(converter.timeUpDown);
        }
    }
}
