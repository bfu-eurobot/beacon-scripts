#!/usr/bin/env python3
from argparse import ArgumentParser
import asyncio
from datetime import datetime
import logging
from typing import List
from aioserial import AioSerial
from aioredis import ConnectionPool, Redis

logging.basicConfig(format="%(levelname)-5s %(message)s", force=True)
log = logging.getLogger()
log.setLevel(logging.INFO)

async def read(pool: ConnectionPool, keys: List[str], out: asyncio.Queue[int], poll_rate: float):
    log.warning(f"Polling REDIS Keys ({keys}) for int value of balls!")
    last: int = 0
    async def get_from(key: str):
        try:
            redis = Redis(connection_pool=pool)
            return int(await redis.get(key))
        except: return 0
    while True:
        try:
            all_balls = sum(await asyncio.gather(*map(get_from, keys)))
            if all_balls is None:
                await asyncio.sleep(poll_rate)
                continue
            if all_balls != last:
                log.info(f"Received balls count: {int(all_balls)}")
                out.put_nowait(all_balls)
            last = all_balls
        except Exception as e:
            log.error(f"While polling {keys}:")
            log.exception(e)
        finally:
            await asyncio.sleep(poll_rate)

async def write(dev: AioSerial, input: asyncio.Queue[int]):
    while True:
        raw = str(await input.get())
        input.task_done()
        raw = "0" * (4 - len(raw)) + raw
        data = bytearray()
        for char in raw:
            data += int(char).to_bytes(1, "little")
        if len(data) > 4:
            log.error(f"Number too big")
            continue
        payload = data + b"\xff\xff\xff"
        log.info(f"Sending {payload}")
        await dev.write_async(payload)

async def start_lift(dev: AioSerial, pool: ConnectionPool, key: str, poll_rate: float):
    log.warning(f"Listening REDIS Channel ({key}) for any message to start lift!")
    sleep = lambda: asyncio.sleep(poll_rate)
    redis = Redis(connection_pool=pool).pubsub()
    await redis.subscribe(key)
    while True:
        try:
            msg = await redis.get_message()
            if msg is None or msg["type"] != "message": 
                await sleep()
                continue
            as_split = msg["data"].decode().split("-")
            time = (int(as_split[0]), int(as_split[1]))
            log.warning(f"STARTING LIFT for ms: {time}!!!! at {datetime.now().isoformat()}")
            await dev.write_async(time[0].to_bytes(2, "little") + time[1].to_bytes(2, "little") + b"\xfa\xfa\xfa")
        except Exception as e:
            log.error(f"While polling {key}:")
            log.exception(e)
        finally:
            await sleep()
            
if __name__ == "__main__":
    parser = ArgumentParser(prog="Redis Key to Serial commands")
    parser.add_argument("from_keys", help="Redis key to listen to", nargs='+', metavar='N')
    parser.add_argument("--to", required=True, help="Com port to write", dest="to")
    parser.add_argument("--baud", required=False, default=9600, type=int, dest="baud")
    parser.add_argument("--lift_channel", required=True, dest="lift_channel")
    parser.add_argument("--poll_rate", required=False, default=300, type=int, dest="rate")
    parser.add_argument("--host", required=False, default="localhost", dest="host")
    parser.add_argument("--port", required=False, type=int, default=6379, dest="port")
    args = parser.parse_args()
    q = asyncio.Queue()
    pool = ConnectionPool.from_url(f"redis://{args.host}:{args.port}")
    dev = AioSerial(port=args.to, baudrate=args.baud)
    log.info(f"Opening port {args.to} at baud {args.baud}")
    if not dev.is_open:
        raise RuntimeError(f"Could not open port {args.to} at baud {args.baud}")
    async def _entry():
        await asyncio.sleep(3.5) # arduino bootup time
        await asyncio.gather(
            write(dev, q), 
            read(pool, args.from_keys, q, args.rate / 1000.), 
            start_lift(dev, pool, args.lift_channel, args.rate / 1000.)
        )
    asyncio.run(_entry())
